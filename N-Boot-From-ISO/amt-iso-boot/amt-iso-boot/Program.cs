﻿using System;
using Intel.Manageability;
using Intel.Manageability.Redirection;
using Intel.Manageability.Exceptions;
using System.Threading;
using Intel.Manageability.BootControl;

namespace amt_iso_boot
{
    class Program
    {
        // Exit Codes Types
        private enum exitCodes
        {
            EXIT_SUCCESS = 0,
            EXIT_FAILURE = -1,
            EXIT_USAGE = -2,
            EXIT_COMMUNICATION_ERROR = -3,
            EXIT_ARGUMENT_ERROR = -4,
        }

        static int Main(string[] args)
        {
			IAMTInstance amt;
			
            if(args.Length != 4)
            {
                Console.WriteLine("Usage: ./amt-iso-boot <host> <user> <password> <ISO>");
                Console.WriteLine("Example: ./amt-iso-boot 192.168.192.21 admin P@assW0rd linux-liveCD.iso");
                return (int)exitCodes.EXIT_USAGE;
            }

            string host = args[0];
            string username = args[1];
            string password = args[2];
            string iso = args[3];
     
            try
            {
                // Connect to AMT
                Console.Write("Connecting to " + host + "...");
                ConnectionInfoEX ci = new ConnectionInfoEX(host, username, password, false,
                                                           string.Empty, ConnectionInfoEX.AuthMethod.Digest,
                                                           null, null, null);
                amt = AMTInstanceFactory.CreateEX(ci);
                Console.WriteLine("Connected!");

                // Enable the IDER interface
                Console.Write("Enabling IDE-R interface...");
                amt.RedirectionIDER.SetInterfaceState(true);
                Console.WriteLine("Done!");

                // Starts IDE CD redirection
                Console.Write("Setting boot source to " + iso + "...");
                amt.RedirectionIDER.StartIDERCD(iso, RedirectState.IDER_SET_IMMEDIATELY);

                // Set boot source
                BootSource bootSource = new BootSource(BootSourceEnum.IDERCD, 0);
                amt.BootControl.SetNextBoot(bootSource);
                Console.WriteLine("Done!");

                // Reboot PC
                Console.Write("Rebooting host...");
                Thread.Sleep(100);
                amt.Power.Reset();
                Console.WriteLine("Done!");

                // Get the current IDER session state
                IDERState IDERstate = amt.RedirectionIDER.CurrentSessionState;

                // Get the IDER statistics
                while (true)
                {
                    IDERStatistics statistics = amt.RedirectionIDER.GetStatistics();
                    Console.Write("\rData Sent: " + statistics.DataSent + "Bytes\t" + "Data Received: " + statistics.DataReceived + "Bytes");
                    Thread.Sleep(1000);
                }
            }

            catch (ManageabilityException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return (int)exitCodes.EXIT_FAILURE;
            }       
        }
    }
}
