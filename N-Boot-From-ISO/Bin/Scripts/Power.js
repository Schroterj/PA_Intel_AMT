//----------------------------------------------------------------------------
//
// Copyright (c) Intel Corporation, 2010  All Rights Reserved.
//
//  Contents:   Demonstrate the PowerOperation COM interface.
//    
//----------------------------------------------------------------------------

var info = new ActiveXObject("Intel.Manageability.ConnectionInfo");
if (typeof (info) != undefined) 
{
    info.Host = "10.0.0.15"; 
    info.UserName = "admin";
    info.Password = "Admin!98"; 
	var PowerState = { "Unknown": 0, "On": 1, "S0": 1, "S3": 2, " StandBy": 2, "Hibernate": 3, "S4": 3, "Off": 4, "S5": 4, "Unexpected": 5  }
	
    var amt = new ActiveXObject("Intel.Manageability.COM.AMTInstance");
    if (typeof (amt) != undefined) 
    {
        try 
		{		
            amt.Create(info);
			
			//Get current power state
			var pState = amt.Power.CurrentPowerState();
			alert("Current state: " + pState + "</br>");
			
			//Power down
            amt.Power.PowerDown();
          
		}
        catch (Error) 
        {
            alert(Error.Message);
        }
    }
    else 
    {
        alert("Failed to perform Power call");
    }
}
else 
{
    alert("Failed to initialize ConnectionInfo object");
}   