//----------------------------------------------------------------------------
//
//  Copyright (C) Intel Corporation, 2010 All Rights Reserved.
//
//  File:       Readme.txt 
//
//  Contents:   Intel(R) Active Management Technology (Intel(R) AMT):
//              A short description of the JavaScripts folder content
//
//----------------------------------------------------------------------------

The SDK provides COM interface for the High Level API.
This folder contains few JavaScripts demonstrate how to use this COM interface.

The features supported via COM so far are:
- Power
- KVMSetup
- AlarmClock
- Redirection

The SDK provide JavaScript for each feature that demonstrate the whole interface of the feature
and not just a single call.
Please read the comments within the scripts in order to understand the differnces between the interfaces
(COM HLA interface and current one).
  