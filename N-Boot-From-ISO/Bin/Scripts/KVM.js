//----------------------------------------------------------------------------
//
// Copyright (c) Intel Corporation, 2010  All Rights Reserved.
//
//  Contents:   Demonstrate the KVM COM interface.
//    
//----------------------------------------------------------------------------
   var Screen = { "Monitor0": 0, "Monitor1": 1 }
   var PortsState = { "DisableAll": 0, "DefaultPortOnly": 1, "RedirectionPortOnly": 2, "EnableAll": 3 }
var info = new ActiveXObject("Intel.Manageability.ConnectionInfo");
if (typeof (info) != undefined) 
{
    info.Host = "10.0.0.15"; 
    info.UserName = "admin";
    info.Password = "Admin!98"; 

    var amt = new ActiveXObject("Intel.Manageability.COM.AMTInstance");
    if (typeof (amt) != undefined) 
    {
        try {
            amt.Create(info);
            amt.KVMSetup.SetDefaultMonitor(Screen.Monitor0);
            amt.KVMSetup.SetRFBPassword("P@ssw0rd");
            amt.KVMSetup.SetOptInTimeout(200);
            amt.KVMSetup.SetSessionTimeout(150);
            amt.KVMSetup.SetOptInPolicy(true);
            amt.KVMSetup.SetPortsState(PortsState.DisableAll);
           
		   //Get capabilities
		   var capabilities = amt.KVMSetup.GetCapabilities();
		   document.write("Is default port enabled: " + capabilities.IsDefaultPortEnable + "</br>");
           document.write("Is redirection port enabled: " + capabilities.IsRedirectionPortEnable + "</br>");		
        }
        catch (Error) 
        {
            alert(Error.Message);
        }
    }
    else 
    {
        alert("Failed to perform KVM call");
    }
}
else 
{
    alert("Failed to initialize ConnectionInfo object");
}   