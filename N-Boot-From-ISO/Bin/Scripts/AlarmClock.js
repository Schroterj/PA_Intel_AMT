
//----------------------------------------------------------------------------
//
// Copyright (c) Intel Corporation, 2010  All Rights Reserved.
//
//  Contents:   Demonstrate the AlarmClock COM interface.
//    
//----------------------------------------------------------------------------   

var info = new ActiveXObject("Intel.Manageability.ConnectionInfo");
if (typeof (info) != undefined) 
{
    info.Host = "10.0.0.15"; 
    info.UserName = "admin";
    info.Password = "Admin!98"; 

   var amt = new ActiveXObject("Intel.Manageability.COM.AMTInstance");
    if (typeof (amt) != undefined) 
    {
        try {
            amt.Create(info);

            // =============================================================
            // Summary: Set single alarm.
            // As JScript dateTime (Date()) does not match .Net DateTime
            // object, the SetSingleAlarm gets the next time as a string.
            // JavaScript Date object supports 3 string formats, but the only
            // format that .Net also supports is: Date.toLocaleString()
            // e.g: Sunday, October 10, 2010 12:00:00 AM  
		    // note: check that the validation of time has to be on the script side
            // ==============================================================
            var nextTime = new Date("10/10/2010");
            amt.AlarmClock.SetSingleAlarm(nextTime.toLocaleString());

            // =============================================================
            // Summary: Set recurring alarm.
            // About nextTime object - see the comment of SetSingleAlarm call
            // interval - should provide the interval as string.
            // It is in the format: PnYnMnDTnHnMnS
            // where:
            // 'P' - desgnates the start of a date string
            // nY - represents the number of years
            // nM - the number of months
            // nD - the number of days
            // 'T' - is the date/time separator (start of a time string)
            // nH - the number of hours
            // nM - the number of minutes
            // nS - the number of seconds. The number of seconds can include decimal digits to an arbitrary precision.
            // Example:
            // P1Y2M3DT10H30M - defines a duration of 1 year, 2 months, 3 days, 10 hours, and 30 minutes
            // Note: That the ONLY parameters that can be set in the interval are: Days, Hours, Minutes 
            //       - Other parameters MUST be 0.     
            // ==============================================================
            nextTime.setFullYear(2012);
            var interval = "P0Y0M1DT5H8M0S";
            amt.AlarmClock.SetRecurringAlarm(nextTime.toLocaleString(), interval);
           
            // Get alarm clock settings
            var settings = amt.AlarmClock.GetAlarmClockSettings();
            var nextAlarmTime = settings.NextAlarmTime;
            var intervalTime = settings.AlarmInterval.toString();
            
            // Disable the alarm clock settings
            amt.AlarmClock.DisableAll();
        }
        catch (Error) 
        {
            alert(Error.Message);
        }
    }
    else 
    {
        alert("Failed to perform AlarmClock call");
    }
}
else 
{
    alert("Failed to initialize ConnectionInfo object");
}   