﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intel.Manageability.Exceptions;
using HLAPI.Services;
using Intel.Manageability;
using System.Threading;

namespace AMT_AgentLocal
{
    class Program
    {
        // Exit Codes Types
        private enum exitCodes
        {
            EXIT_SUCCESS = 0,
            EXIT_FAILURE = -1,
            EXIT_USAGE = -2,
            EXIT_COMMUNICATION_ERROR = -3,
            EXIT_ARGUMENT_ERROR = -4,
        }

        static int Main(string[] args)
        {
            IAMTInstance amt;

            if (args.Length != 4)
            {
                Console.WriteLine("Usage: ./amt-agent-local <user> <password> <agent name> <process>");
                Console.WriteLine("Example: ./amt-agent-local admin P@assW0rd agent1 firefox");
                return (int)exitCodes.EXIT_USAGE;
            }

            string username = args[0];
            string password = args[1];      
            string agentName = args[2];
            string process = args[3];

            try
            {
                // Connect to AMT
                Console.Write("Connecting to local host...");
                ConnectionInfoEX ci = new ConnectionInfoEX("127.0.0.1", username, password, false,
                                                           string.Empty, ConnectionInfoEX.AuthMethod.Digest,
                                                           null, null, null);
                amt = AMTInstanceFactory.CreateEX(ci);
                Console.WriteLine("Connected!");

                // Start Agent Presence
                AgentPresenceLocal.Start(amt, agentName);

                // Start Process Monitor
                AgentPresenceLocal.StartProcessMonitor(amt, process, agentName);

                Console.WriteLine("Press any key to exit...");
                Console.Read();

                // Bye
                return (int)exitCodes.EXIT_SUCCESS;
            }
            catch (ManageabilityException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return (int)exitCodes.EXIT_FAILURE;
            }

            
        }
    }
}
