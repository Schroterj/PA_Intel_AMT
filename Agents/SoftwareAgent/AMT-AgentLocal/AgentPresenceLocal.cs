﻿using Intel.Manageability;
using Intel.Manageability.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMT_AgentLocal
{
    class AgentPresenceLocal
    {
        public static void Start(IAMTInstance amt, string agentName)
        {
            // To start an agent, do the following:
            // 1. Create the Agent by invoking IAMTInstance.AgentPresence.Remote.CreateOrUpdateAgent(string)
            //    with AgentName (see AgentPresenceRemoteSample sample).   
            // 2. From AMT - local, register to watchdog and send heartbeats by invoking amt.AgentPresence.Local.StartPresence().
            // When the StartPresence function sends heartbeats, it activates an endless loop that sends a heartbeat each agent.TimeoutInterval,
            // So, to avoid getting stuck on the StartPresence function, perform it in another thread.

            ThreadPool.QueueUserWorkItem(agent =>
            {
                try
                {
                    Console.WriteLine("Start agent presence: " + agentName.ToString());
                    amt.AgentPresence.Local.StartPresence(agentName);
                }
                catch (ManageabilityException ex)
                {
                    Console.WriteLine("Start agent presence failed with exception: " + ex.Message);
                }
            });

        }

        public static void StartProcessMonitor(IAMTInstance amt, string process, string agentName)
        {
            ThreadPool.QueueUserWorkItem(monitor =>
            {
                try
                {
                    Console.WriteLine("Start monitoring " + process.ToString());
                    while (true)
                    {
                        Process[] pname = Process.GetProcessesByName(process.ToString());
                        if (pname.Length != 0)
                        {
                            Console.Write("\rProcess is running");
                        }
                        else
                        {
                            Console.Write("\nProcess has stopped. Stopping agent...");
                            amt.AgentPresence.Local.ExpirePresence(agentName);
                            Console.WriteLine("Done!");
                            break;
                        }

                        Thread.Sleep(1000);
                    }
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);
                }
            });

        }
        public static void Shutdown(IAMTInstance amt, string agentName)
        {
            try
            {
                amt.AgentPresence.Local.ShutdownPresence(agentName);
                Console.WriteLine("Shutdown agent " + agentName + " completed successfully.");
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("Shutdown agent " + agentName + " failed with exception: " + ex.Message);
            }
        }
    }
}
