﻿using System;
using System.Collections.Generic;
using Intel.Manageability;
using Intel.Manageability.AgentPresence;
using Intel.Manageability.Exceptions;
using System.Collections;

namespace amt_agent_remote
{
    class AgentPresenceRemoteFunctionality
    {
        public static void CreateOrUpdateAgent(IAMTInstance amt, string agentName)
        {
            List<AgentAction> actions = new List<AgentAction>();


            // --------------------
            //    Create Agent
            // --------------------

            // Prepare action list
            AgentAction action = new AgentAction(AgentState.Running, AgentState.Expired | AgentState.Stopped, true);                

            // Add the actions to the action list.
            actions.Add(action);

            // Create policy and pass the action list as a parameter
            Agent agent = new Agent(agentName, actions, 3600, 6);


            // -------------------------------------------
            //    Add the agent to the Intel AMT instance
            // -------------------------------------------

            try
            {
                amt.AgentPresence.Remote.CreateOrUpdateAgent(agent);
                Console.WriteLine("Create agent completed successfully.");
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("Create agent failed with exception: " + ex.Message);
            }
        }

        public static void DeleteAgent(IAMTInstance amt, string agentName)
        {
            try
            {
                amt.AgentPresence.Remote.DeleteAgent(agentName);
                Console.WriteLine("Delete agent completed successfully.");
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("Delete agent failed with exception: " + ex.Message);
            }
        }

        public static void DeleteAllAgent(IAMTInstance amt)
        {
            try
            {
                amt.AgentPresence.Remote.DeleteAllAgents();
                Console.WriteLine("Delete all agents completed successfully.");
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("Delete agents failed with exception: " + ex.Message);
            }
        }

        public static void GetAgent(IAMTInstance amt, string agentName)
        {
            try
            {
                // Retrieve all agents
                Agent agent = amt.AgentPresence.Remote.GetAgent(agentName);
                Console.WriteLine("\nAgent:");
                Console.WriteLine("-------");

                PrintAgentCentent(agent);
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("Display agent failed with exception: " + ex.Message);
            }
        }

        //For AMT > 11.0
        public static void SetExpirationActionOnAgentPresenceWatchDog(IAMTInstance amt, string agentName)
        {
            try
            {
                //set expiration action properties
                WatchDogExpirationAction expirationAction = new WatchDogExpirationAction(ExpirationAction.Reboot, ExpirationAction.Reboot, 60, true);
                amt.AgentPresence.Remote.SetExpirationAction(expirationAction);
                //apply expiration action on specific agent
                amt.AgentPresence.Remote.ApplyActionOnWatchDog(agentName, true);
                Console.WriteLine("Set expiration action completed successfully.");
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("Set expiration action failed with exception: " + ex.Message);
            }
        }

        private static void PrintAgentCentent(Agent agent)
        {
            Console.WriteLine("Agent Name: {0} \n", agent.ApplicationName);
            Console.WriteLine("\tDevice ID: {0} ", agent.DeviceID);
            Console.WriteLine("\tState: {0} ", agent.CurrentState);
            Console.WriteLine("\tStartup Interval: {0} ", agent.StartupInterval);
            Console.WriteLine("\tTimeout Interval: {0} ", agent.TimeoutInterval);

            // Go over the actions and display their capabilities
            IEnumerator actionEnumerator = agent.Actions.GetEnumerator();
            Console.WriteLine("Actions:");
            while (actionEnumerator.MoveNext())
            {
                PrintAction(actionEnumerator.Current as AgentAction);
            }
        }

        private static void PrintAction(AgentAction action)
        {
            Console.WriteLine("\n\t* Old State: {0} ", action.OldState);
            Console.WriteLine("\t  New State: {0} ", action.NewState);
            Console.WriteLine("\t  Create Event: {0} ", action.EventOnTransition);
            Console.WriteLine("\t  System Defense Action: {0} ", action.ActionSystemDefense);
            Console.WriteLine("\t  EAC Action: {0} \n", action.ActionEAC);
        }
    }
}
