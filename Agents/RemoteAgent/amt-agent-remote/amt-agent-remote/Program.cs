﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intel.Manageability;
using Intel.Manageability.Exceptions;
using HLAPI.Services;
using System.Net;

namespace amt_agent_remote
{
    class Program
    {
        // Exit Codes Types
        private enum exitCodes
        {
            EXIT_SUCCESS = 0,
            EXIT_FAILURE = -1,
            EXIT_USAGE = -2,
            EXIT_COMMUNICATION_ERROR = -3,
            EXIT_ARGUMENT_ERROR = -4,
        }

        static WSEventListener listener;
        static readonly int PORT = 12345;
        static IAMTInstance amt;
        static string host;
        static string username;
        static string password;
        static string agentName;

        static int Main(string[] args)
        {
            
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: ./amt-agent-remote <host> <user> <password> <agent name>");
                Console.WriteLine("Example: ./amt-agent-remote 192.168.192.21 admin P@assW0rd agent1");
                return (int)exitCodes.EXIT_USAGE;
            }

            host = args[0];
            username = args[1];
            password = args[2];
			agentName = args[3];



            try
            {
                // Connect to AMT
                Console.Write("Connecting to " + host + "...");
                ConnectionInfoEX ci = new ConnectionInfoEX(host, username, password, false,
                                                           string.Empty, ConnectionInfoEX.AuthMethod.Digest,
                                                           null, null, null);
                amt = AMTInstanceFactory.CreateEX(ci);
                Console.WriteLine("Connected!");

                // Create Agent
                AgentPresenceRemoteFunctionality.CreateOrUpdateAgent(amt, agentName);

                // View informations about the agent
                AgentPresenceRemoteFunctionality.GetAgent(amt, agentName);

                // Register to WS-Event -> so we know when the Agent state change
                WSEventingFunctionality.Subscribe(amt, WSEventingFunctionality.GetLocalIPAddress(), PORT);

                // Display current subscription
                WSEventingFunctionality.DisplayCurrentSubscription(amt);

                // Now we can listening for Agent state change 
                Console.WriteLine("Start Listening on port {0}, Press any key to exit...", PORT);
                listener = new WSEventListener(IPAddress.Any, PORT);
                listener.OnNewEventArrived += new EventHandler<WSEventArgs>(listener_OnNewEventArrived);
                listener.StartListening();
                Console.ReadLine();

                // Stop the listener
                listener.StopListening();

                // Remove agent
                AgentPresenceRemoteFunctionality.DeleteAgent(amt, agentName);

                // Unsubscribe
                WSEventingFunctionality.UnSubscribe(amt, WSEventingFunctionality.GetLocalIPAddress(), PORT);

                // Bye
                return (int)exitCodes.EXIT_SUCCESS;
            }
            catch (ManageabilityException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return (int)exitCodes.EXIT_FAILURE;
            }


        }

        static void listener_OnNewEventArrived(object sender, WSEventArgs e)
        {
            // Handle the event data...
            Console.WriteLine("Address: " + e.Sender + " Message: " + e.EventData.MessageDescription);
            Console.WriteLine("Alerting element format: " + e.EventData.AlertingElementFormat);
            Console.WriteLine("Message ID: " + e.EventData.MessageID);
            Console.WriteLine("Message args: " + e.EventData.MessageArguments);
            Console.WriteLine("Filter Name: " + e.EventData.IndicationFilterName);
            Console.WriteLine("Alert Type: " + e.EventData.AlertType);

            // Shutdown the machine
            if (e.Sender.ToString().Equals(host))
                Console.WriteLine("Perform AMT action...");
                //amt.Power.Sleep();
                //amt.Power.PowerDown();
        }
    }
}
