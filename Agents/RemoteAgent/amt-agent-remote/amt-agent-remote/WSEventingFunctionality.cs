﻿using Intel.Manageability;
using Intel.Manageability.Events;
using Intel.Manageability.Exceptions;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.Xml;

namespace amt_agent_remote
{
    class WSEventingFunctionality
    {
        public static void Subscribe(IAMTInstance amt, string remoteIP, int port)
        {
            string address = "http://" + remoteIP + ":" + port.ToString();
            try
            {
                //Agent is in "Features" filter
                if (amt.Events.WSEvents.supportedFilters.Contains(FilterName.Features))
                {
                    //Subscribe with the default (PUSH) delivery mode
                    //The address (FQDN or IP format) must begin with ‘http://’ ('https://' is also supported from AMT version 11.0 and above) and include the port.
                    Subscription subscription = new Subscription(address, FilterName.Features, SenderIDType.CurrentAddress);
                    //the SenderId will be added to ReferenceParameter, in Intel(R) AMT < 5.1 it will be added to the HTTPHeader
                    subscription.SenderIDPlacing = SenderIDPlacing.ReferenceParameter;
                    amt.Events.WSEvents.Subscribe(subscription, null, null);
                    Console.WriteLine("\nSubscription operation completed successfully.");
                }
                else
                {
                    Console.WriteLine("\n" + "This filter is not supported by this version of Intel(R) AMT ");
                }
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

        public static void UnSubscribe(IAMTInstance amt, string remoteIP, int port)
        {
            string address = "http://" + remoteIP + ":" + port.ToString();

            try
            {
                bool Unsubscribed = false;
                Collection<Subscription> subscriptions = amt.Events.WSEvents.EnumerateSubscriptions();
                foreach (Subscription sub in subscriptions)
                {
                    //Delete all the subscriptions with listenerAddress == address, filter == Features, DeliveryMode = Push
                    if ((sub.DeliveryMode == DeliveryMode.Push) && (sub.ListenerAddress == address) && (sub.Filter == FilterName.Features))
                    {
                        amt.Events.WSEvents.UnSubscribe(sub);
                        Unsubscribed = true;
                    }
                }
                if (!(Unsubscribed))
                {
                    Console.WriteLine("Subscription not found.");
                    return;
                }
                Console.WriteLine("\nUnSubscribe operation completed successfully.");
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

        public static void DisplayCurrentSubscription(IAMTInstance amt)
        {
            try
            {
                Collection<Subscription> subscriptions = amt.Events.WSEvents.EnumerateSubscriptions();

                Console.WriteLine("Subscriptions");
                Console.WriteLine("-------------");

                foreach (Subscription sub in subscriptions)
                {
                    Console.WriteLine("\nListener address: {0}", sub.ListenerAddress);
                    Console.WriteLine("DeliveryMode: {0}", sub.DeliveryMode);
                    Console.WriteLine("Type of filter: {0}", sub.Filter);
                    Console.WriteLine("Sender address: {0}", sub.Sender);
                }
            }
            catch (ManageabilityException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (ip.ToString().Contains("192.168"))
                        return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
